package net.palmund.query;

import database.Schema;
import evaluation.Predicate;
import query.AbstractEnumerator;
import query.Enumerator;
import storage.Record;

/**
 * Created by palmund on 21/03/14.
 */
public abstract class AbstractJoinEnumerator extends AbstractEnumerator {
    protected final Enumerator joiningSource;
    protected final Predicate operation;
    protected final boolean validMergedSchema;

    private final Schema mergedSchema;

    protected Record saved;

    public AbstractJoinEnumerator(Enumerator source, Enumerator joiningSource, Predicate operation) {
        super(source);
        this.joiningSource = joiningSource;
        this.operation = operation;
        this.mergedSchema = source.getSchema().merge(joiningSource.getSchema());
        this.validMergedSchema = operation == null || operation.validate(mergedSchema);
    }

    @Override
    public void open() {
        super.open();
        joiningSource.close();
    }

    @Override
    public Schema getSchema() {
        return this.mergedSchema;
    }

    @Override
    protected Record fetchNext() {
        return null;
    }
}