package net.palmund.query;

import evaluation.Predicate;
import query.Enumerator;
import storage.Record;

import java.util.HashMap;

/**
 * Created by palmund on 19/03/14.
 */
public class LeftOuterJoinEnumerator extends AbstractJoinEnumerator {
    private final Counter<Record> counter = new Counter<>();

    public LeftOuterJoinEnumerator(Enumerator source, Enumerator joiningSource, Predicate operation) {
        super(source, joiningSource, operation);
    }

    @Override
    protected Record fetchNext() {
        //<editor-fold desc="Bulskov">
        if (!validMergedSchema) {
            return null;
        }
        // done with the inner loop;
        if (!joiningSource.hasNext()) {
            saved = null;
        }
        // don't have a row for the outer loop
        if (saved == null) {
            if (source.hasNext()) {
                saved = source.next();
                joiningSource.rewind();
            } else // outer loop is finished
            {
                return null;
            }
        }
        //</editor-fold>

        // join
        while (joiningSource.hasNext()) {
            Record record = joiningSource.next();

            Record joinedRecord = saved.join(record, getSchema());
            if (operation == null || operation.evaluate(joinedRecord, getSchema())) {
                counter.countUp(saved);
                return joinedRecord;
            }
        }

        if (counter.getCount(saved) == 0) {
            return saved.padAccordingToSchema(getSchema());
        }
        if (source.hasNext()) {
            return fetchNext();
        }
        return null;
    }

    private class Counter<T> {
        private HashMap<T, Integer> counts = new HashMap<>();

        public void countUp(T saved) {
            Integer integer = counts.get(saved);
            if (integer == null) {
                integer = new Integer(0);
            }
            integer++;
            counts.put(saved, integer);
        }

        public int getCount(T saved) {
            Integer integer = counts.get(saved);
            if (integer == null) {
                integer = new Integer(0);
            }
            return integer;
        }
    }
}