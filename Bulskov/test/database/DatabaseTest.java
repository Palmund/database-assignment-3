/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package database;

import org.junit.Before;
import org.junit.Test;
import storage.DbIterator;

/**
 *
 * @author bulskov
 */
public class DatabaseTest
{
    
    public DatabaseTest()
    {
    }
    
    @Before
    public void setUp()
    {
    }
    
    @Test
    public void testTableScanner()
    {
        Database db = new Database();
        
//        db.createTable("test", 
//                new Attribute[] { 
//                    Attribute.createString("a", 30),
//                    Attribute.createInteger("b")
//                });
//        
//        db.insertInto("test", "hello1", 5);
//        db.insertInto("test", "hello2", 6);
//        db.insertInto("test", "hello3", 7);
//        db.insertInto("test", "hello4", 8);
//        db.commit();
        
        //db.close();
        //Database db2 = new Database();
        
//        DbIterator columnScanner = db.getTableScanner("sys_column");
//        columnScanner.open();
//        while(columnScanner.hasNext())
//            System.out.println(columnScanner.next());
        
        DbIterator tableScanner = db.getTableScanner("test");
        tableScanner.open();
        while(tableScanner.hasNext())
            System.out.println(tableScanner.next());
      
    }
    
}
