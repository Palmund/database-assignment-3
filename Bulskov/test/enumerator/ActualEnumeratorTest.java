/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package enumerator;

import database.Database;
import evaluation.LogicalOperation;
import org.junit.Assert;
import org.junit.Test;
import query.Enumerator;

import static evaluation.Operations.*;

/**
 *
 * @author palmund
 */
public class ActualEnumeratorTest
{
    Database db = new Database();

    @Test
    public void testSelectWhere()
    {
        Enumerator query = db.from("sys_column")
                             .select("table_name", "column_name")
                             .where(column("table_name").isEqualTo(string("sys_column")))
                             .where(column("column_name").isEqualTo(string("data_type")))
                ;
        query.open();

        int i = 0;
        while(query.hasNext()) {
            query.next();
            i += 1;
        }

        Assert.assertEquals("More than one record found", 1, i);
    }

    @Test
    public void testLogicalOperation()
    {
        Enumerator query = db.from("sys_relation")
                ;
        query.open();

        int i = 0;
        while(query.hasNext()) {
            query.next();
            i += 1;
        }

        Assert.assertTrue("The 3 default columns were not found", 3 <= i); // There may be more than 3 but 3 verifies that "sys_relation", "sys_column", and "sys_config" is present.
    }

    @Test
    public void testSimpleJoin()
    {
        LogicalOperation op1 = field(0).isEqualTo(field(2));
        LogicalOperation op2 = column("sys_relation.table_name").isEqualTo(column("sys_column.table_name"));

        Enumerator query = db.from("sys_relation")
                             .join(db.from("sys_column"))
                                .on(op2)
//                             .select("dbfile")
                             .select("sys_relation.table_name", "dbfile", "column_name")
                ;
        query.open();

        int i = 0;
        while(query.hasNext()) {
            query.next();
            i += 1;
        }

        Assert.assertTrue("Not all system data is present", 9 <= i); // Verifies that all the system data is present.
    }
}