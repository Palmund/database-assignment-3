/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package enumerator;

import database.Database;
import evaluation.LogicalOperation;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import query.Enumerator;

import static evaluation.Operations.*;

/**
 *
 * @author bulskov
 */
public class EnumeratorTest
{
    Database db = new Database();

    public EnumeratorTest()
    {
    }

    @Before
    public void setUp()
    {
    }

    @Test
    @Ignore
    public void testSelectWhere()
    {
        System.out.println("====testSelectWhere====");
        Enumerator query = db.from("sys_column")
                             .select("table_name", "column_name")
                             .where(column("table_name").isEqualTo(string("sys_column")))
                             .where(column("column_name").isEqualTo(string("data_type")))
                ;
        query.open();
        int i = 0;
        while(query.hasNext()) {
            i += 1;
            System.out.println(query.next());
        }
        Assert.assertEquals(1, i);
        System.out.println("");
    }

    @Test
    public void testLogicalOperation()
    {
        Enumerator query = db.from("sys_relation")
                ;
        query.open();
        while(query.hasNext())
            System.out.println(query.next());
    }

    @Test
    public void testSimpleJoin()
    {
        System.out.println("====testSimpleJoin====");

        LogicalOperation op1 = field(0).isEqualTo(field(2));
        LogicalOperation op2 = column("sys_relation.table_name").isEqualTo(column("sys_column.table_name"));

        Enumerator query = db.from("sys_relation")
                             .join(db.from("sys_column"))
                                .on(op2)
//                             .select("dbfile")
                             .select("sys_relation.table_name", "dbfile", "column_name")
                ;
        query.open();

        int i = 0;
        while(query.hasNext()) {
            i += 1;
            System.out.println(query.next());
        }
        Assert.assertEquals(2, i);
        System.out.println("");
    }

}
