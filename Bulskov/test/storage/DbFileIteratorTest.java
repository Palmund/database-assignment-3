/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package storage;

import database.Database;
import database.Schema;
import java.io.IOException;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import util.Mother;

/**
 *
 * @author bulskov
 */
public class DbFileIteratorTest {
    Database database;
    Schema schema;
    DbFile file;
    final String tableName = "test";

    @Before
    public void setUp() throws IOException {
        if(database != null)
            database.dropTable(tableName);
        database = new Database(5);
        database.createTable(tableName, Mother.createAttributes());
        schema = database.getSchema(tableName);
        file =  database.getFile(tableName);
    }
    
//    @Test
//    public void testIterator() throws IOException {
//        Record rec1 = Mother.createRecord();
//        Record rec2 = Mother.createRecord("testing", 4, 2.7);
//        
//        
//        
//        //Block block1 = file.allocateBlock();
//        //int slot1 = block1.getNextFreeSlot();
//        //block1.insert(slot1, rec1);
//        //file.writeBlock(block1);
//
//        Block block2 = file.allocateBlock();
//        int slot2 = block2.getNextFreeSlot();
//        block2.insert(slot2, rec2);
//        file.writeBlock(block2);
//        file.flush();
//
//        DbFile sameFile = new DbFile("test.db", schema, false);
//        DbFileIterator iterator = sameFile.iterator(database.getBufferManager());
//        iterator.open();
//        while (iterator.hasNext()) {
//            System.out.println(iterator.next());
//        }
//    }
//    
//    @Test
//    public void testFreeSpace() throws IOException
//    {
//        Block block;
//        for(int i = 0; i < 10; i++)
//        {
//            block = file.allocateBlock();
//            while(block.hasFreeSpace())
//            {
//                int slot = block.getNextFreeSlot();
//                Record rec = Mother.createRecord("hello"+i+slot, (i+slot), (i+slot)*1.2);
//                block.insert(slot, rec);
//            }
//            assertFalse(block.hasFreeSpace());
//            file.writeBlock(block);
//        }
//        DbFileIterator iterator = file.iterator(database.getBufferManager());
//        iterator.open();
//        while(iterator.hasNext())
//            iterator.next();
//        
//        iterator.rewind();
//        while(iterator.hasNext())
//            iterator.next();
//        
//        System.out.println("readCount: " + database.getBufferManager().getReadCount());
//        System.out.println("totalCount: " + database.getBufferManager().getTotalCount());
//    }
    
}
