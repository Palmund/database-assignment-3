/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package storage;

import database.Schema;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.junit.Test;
import static org.junit.Assert.*;
import static storage.Block.BLOCK_SIZE;
import util.Mother;

/**
 *
 * @author bulskov
 */
public class RecordTest {
    
    @Test
    public void recordShouldReadAndWriteToBuffer() throws IOException
    {
        Schema schema = Mother.createSchema();
        Record rec = Mother.createRecord();
        rec.setRecordId(new RecordId(new BlockId(1, 2), 3));
        
        ByteArrayOutputStream baos = new ByteArrayOutputStream(BLOCK_SIZE);
        DataOutputStream dos = new DataOutputStream(baos);
        rec.write(dos);
        
        DataInputStream dis = new DataInputStream(new ByteArrayInputStream(baos.toByteArray()));
        Record savedRed = new Record(schema, dis);
        
        assertEquals(rec.getRecordId(), savedRed.getRecordId());
        assertEquals(rec.getData(0), savedRed.getData(0));
        assertEquals(rec.getData(1), savedRed.getData(1));
        assertEquals(rec.getData(2), savedRed.getData(2));
    }
}
