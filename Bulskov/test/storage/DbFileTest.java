/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package storage;

import database.Database;
import database.Schema;
import java.io.IOException;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import util.Mother;

/**
 *
 * @author bulskov
 */
public class DbFileTest {
    Database database;
    DbFile file;
    Schema schema;
    final String tableName = "test";

    @Before
    public void setUp() throws IOException {
        if(database != null)
            database.dropTable(tableName);
        database = new Database(20);
        database.createTable(tableName, Mother.createAttributes());
        file =  database.getFile(tableName);
        schema = database.getSchema(tableName);
    }

//    @Test
//    public void foo() throws IOException {
//        file.flush();
//
//        DbFile sameFile = new DbFile("test.db", schema, false);
//
//        assertEquals(schema.getId(), sameFile.getSchema().getId());
//
//    }
//
//    @Test
//    public void readWriteOneBlock() throws IOException {
//        Record rec = Mother.createRecord();
//        Block block = file.allocateBlock();
//        int slot = block.getNextFreeSlot();
//        block.insert(slot, rec);
//        file.writeBlock(block);
//        file.flush();
//
//        DbFile sameFile = new DbFile("test.db", schema, false);
//        Block readBlock = sameFile.readBlock(block.getId());
//        Record record = readBlock.getRecord(slot);
//
//        assertEquals(rec.getData(0), record.getData(0));
//        assertEquals(rec.getData(1), record.getData(1));
//        assertEquals(rec.getData(2), record.getData(2));
//    }
//
//    @Test
//    public void readWriteTwoBlocks() throws IOException {
//        Record rec1 = Mother.createRecord();
//        Record rec2 = Mother.createRecord("testing", 4, 2.7);
//        Block block1 = file.allocateBlock();
//        int slot1 = block1.getNextFreeSlot();
//        block1.insert(slot1, rec1);
//        file.writeBlock(block1);
//
//        Block block2 = file.allocateBlock();
//        int slot2 = block2.getNextFreeSlot();
//        block2.insert(slot2, rec2);
//        file.writeBlock(block2);
//        file.flush();
//
//        DbFile sameFile = new DbFile("test.db", schema, false);
//        Block readBlock1 = sameFile.readBlock(block1.getId());
//        Record record1 = readBlock1.getRecord(slot1);
//
//        Block readBlock2 = sameFile.readBlock(block2.getId());
//        Record record2 = readBlock2.getRecord(slot2);
//
//        assertEquals(rec1.getData(0), record1.getData(0));
//        assertEquals(rec1.getData(1), record1.getData(1));
//        assertEquals(rec1.getData(2), record1.getData(2));
//
//        assertEquals(rec2.getData(0), record2.getData(0));
//        assertEquals(rec2.getData(1), record2.getData(1));
//        assertEquals(rec2.getData(2), record2.getData(2));
//    }
//    
//    @Test
//    public void testFreeSpace() throws IOException
//    {
//        Block block;
//        for(int i = 0; i < 10; i++)
//        {
//            block = file.allocateBlock();
//            while(block.hasFreeSpace())
//            {
//                int slot = block.getNextFreeSlot();
//                Record rec = Mother.createRecord("hello"+(i+slot), (i+slot), (i+slot)*1.2);
//                block.insert(slot, rec);
//            }
//            assertFalse(block.hasFreeSpace());
//            file.writeBlock(block);
//        }
//        assertEquals(10, file.getNumberOfBlocks());
//        
//        Block readBlock = file.readBlock(new BlockId(file.getSchema().getId(), 3));
//        readBlock.delete(7);
//        file.writeBlock(readBlock);
//        
//        readBlock = file.readBlock(new BlockId(file.getSchema().getId(), 5));
//        readBlock.delete(8);
//        file.writeBlock(readBlock);
//        
//        readBlock = file.getNextFreeBlock();
//        
//        assertEquals(5, readBlock.getId().getBlockId());
//        int nextFreeSlot = readBlock.getNextFreeSlot();
//        readBlock.insert(nextFreeSlot, Mother.createRecord());
//        file.writeBlock(readBlock);
//        
//        assertEquals(3, file.getNextFreeBlock().getId().getBlockId());
//       
//    }
}
