/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package storage;

import database.Schema;
import java.io.IOException;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import util.Mother;

/**
 *
 * @author bulskov
 */
public class BlockTest {

    Schema schema;
    private Block block;
    private Object[] recData;
    
    @Before
    public void setUp() throws IOException {
        
        schema = Mother.createSchema();
        block = new Block(new BlockId(schema.getId(), 0), schema);
        recData = new Object[]{new String("hello"), new Integer(3), new Float(4.3)};
    }

    @Test
    public void blockToByteArrayShouldCreateValidHeaderData() throws IOException {
        int size = block.getFreeSpace();

        byte[] data = block.toByteArray();

        Block savedBlock = new Block(data, schema);

        assertEquals(size, savedBlock.getFreeSpace());
    }

    @Test
    public void blockShouldSaveRecords() throws IOException {
        int size = block.getFreeSpace();
        Record rec = Mother.createRecord(schema, recData);
        int nextFreeSlot = block.getNextFreeSlot();
        block.insert(nextFreeSlot, rec);
        byte[] data = block.toByteArray();
        Block savedBlock = new Block(data, schema);
        assertEquals(size - 1, savedBlock.getFreeSpace());
    }

    @Test
    public void blockShouldSaveRecordsWithCorrectData() throws IOException {
        int size = block.getFreeSpace();
        Record rec = Mother.createRecord(schema, recData);
        int slot = block.getNextFreeSlot();
        block.insert(slot, rec);
        byte[] data = block.toByteArray();
        Block savedBlock = new Block(data, schema);

        Record savedRec = savedBlock.getRecord(slot);

        assertEquals(rec.getData(0), savedRec.getData(0));
        assertEquals(rec.getData(1), savedRec.getData(1));
        assertEquals(rec.getData(2), savedRec.getData(2));
    }

    @Test
    public void blockShouldBeAbleToDeleteRecords() throws IOException {
        for (int i = 0; i < 5; i++) {
            Record rec = Mother.createRecord(schema, recData);
            int slot = block.getNextFreeSlot();
            block.insert(slot, rec);
        }

        int size = block.getFreeSpace();

        block.delete(2);
        
        byte[] data = block.toByteArray();
        Block savedBlock = new Block(data, schema);

        assertEquals(size + 1, savedBlock.getFreeSpace());

    }
    
}
