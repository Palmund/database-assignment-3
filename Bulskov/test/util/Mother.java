/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package util;

import catalog.Attribute;
import catalog.Relation;
import database.Schema;
import storage.FieldFactory;
import storage.FloatField;
import storage.IntegerField;
import storage.Record;
import storage.StringField;

/**
 *
 * @author bulskov
 */
public class Mother {
    public static Schema createSchema()
    {
        Schema schema = new Relation("test");
        schema.addAttribute(Attribute.createString("a", 30));
        schema.addAttribute(Attribute.createInteger("b"));
        schema.addAttribute(Attribute.createFloat("c"));
        return schema;
    }
    
    public static Attribute[] createAttributes()
    {
        return new Attribute[] {
            Attribute.createString("a", 30),
            Attribute.createInteger("b"),
            Attribute.createFloat("c")
        };
    }
    
    public static Record createRecord()
    {
        Object[] data = createRecordData();
        return createRecord(null, data);
    }
    
    public static Record createRecord(Schema schema, Object[] data)
    {
        return createRecord((String)data[0],(int)data[1], (float)data[2]);
    }
    
    public static Record createRecord(String str, int ival, double f)
    {
        Schema schema = createSchema();
        Record rec = new Record(schema);
        rec.setField(0, new StringField(str, schema.getAttribute(0).getLength()));
        rec.setField(1, new IntegerField(ival));
        rec.setField(2, new FloatField((float)f));
        return rec;
    }
    
    public static Object[] createRecordData()
    {
        // just some dummy data
        return createRecordData("hello", 2, 4.3);
    }

    public static Object[] createRecordData(String str, int i, double f) {
        Object[] data = new Object[]{str, new Integer(i), new Float(f)};
        return data;
    }
 }
