package query;

import database.Schema;
import evaluation.LogicalOperation;
import storage.Record;

/**
 *
 * @author bulskov
 */
public class SimpleJoinEnumerator extends AbstractEnumerator
{
    protected final Enumerator joiningSource;
    protected final boolean valid;
    protected final Schema schema;
    protected LogicalOperation operation;
    protected Record saved;
    
    public SimpleJoinEnumerator(Enumerator source, Enumerator joiningSource, LogicalOperation operation)
    {
        super(source);
        this.joiningSource = joiningSource;
        this.operation = operation;
        schema = source.getSchema().merge(joiningSource.getSchema());
        valid = operation == null ? true : operation.validate(getSchema());
    }

    public SimpleJoinEnumerator(Enumerator source, Enumerator joiningSource) {
        super(source);
        this.joiningSource = joiningSource;
        schema = source.getSchema().merge(joiningSource.getSchema());
        valid = operation == null ? true : operation.validate(getSchema());
    }

    public Enumerator on(LogicalOperation operation) {
        this.operation = operation;
        return new VoidEnumerator(this);
    }

    @Override
    protected Record fetchNext()
    {
        if(!valid)
            return null;
        // done with the inner loop;
        if(!joiningSource.hasNext())
            saved = null;
        // don't have a row for the outer loop
        if(saved == null)
        {
            if(source.hasNext()) 
            {
                saved = source.next();
                joiningSource.rewind();
            }
            else // outer loop is finished
            {
                return null;
            }
        }
        // join
        while(joiningSource.hasNext()) {
            Record record = joiningSource.next();
            Record joinedRecord = saved.join(record, schema);
            if (operation == null || operation.evaluate(joinedRecord, schema))
                return joinedRecord;
        }
        if(source.hasNext())
            return fetchNext();
        return null;
    }

    @Override
    public void open()
    {
        super.open(); 
        joiningSource.open();
    }

    @Override
    public Schema getSchema()
    {
        return schema;
    }

    private static class VoidEnumerator extends AbstractEnumerator {
        private final SimpleJoinEnumerator simpleJoinEnumerator;

        public VoidEnumerator(SimpleJoinEnumerator simpleJoinEnumerator) {
            super(simpleJoinEnumerator);
            this.simpleJoinEnumerator = simpleJoinEnumerator;
        }

        @Override
        protected Record fetchNext() {
            return simpleJoinEnumerator.fetchNext();
        }
    }
}