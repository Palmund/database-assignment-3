/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package query;

import Zql.*;
import database.Database;
import database.DbException;
import database.Schema;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author bulskov
 */
public class SQLParser
{
    private final Database database;
    
    public SQLParser(Database database)
    {
        this.database = database;
    }
    
    public Enumerator processStatement(String statement)
    {
        try
        {
            return processStatement(new ByteArrayInputStream(statement.getBytes("UTF-8")));
        } catch (UnsupportedEncodingException ex)
        {
            Logger.getLogger(SQLParser.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public Enumerator processStatement(InputStream is)
    {
        try
        {
            ZqlParser parser = new ZqlParser(is);
            ZStatement statement = parser.readStatement();
            if(statement instanceof ZQuery)
                return handleQueryStatement((ZQuery)statement);
        } catch (ParseException ex)
        {
            Logger.getLogger(SQLParser.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private Enumerator handleQueryStatement(ZQuery query)
    {
        if(query.getFrom().size() > 1) // ups, joins
            throw new DbException("SQLParse.handleQueryStatement: can't handle joins :-)");
        if(query.getWhere() != null)
            throw new DbException("SQLParse.handleQueryStatement: can't handle where :-)");
        Vector<ZSelectItem> selectList = query.getSelect();
        if(selectList.size() == 1 && selectList.get(0).getColumn().equals("*"))
        {
            List<Schema> fromTables = getFromTables(query.getFrom());
            Schema schema = fromTables.get(0);
            String[] columns = new String[schema.numberOfAttributes()];
            for(int i = 0; i < schema.numberOfAttributes(); i++)
                columns[i] = schema.getAttribute(i).getName();
            return database.from(schema.getName())
                           .select(columns);
        }
        String[] columns = new String[selectList.size()];
        for (int i = 0; i < selectList.size(); i++) {
            ZSelectItem si = selectList.elementAt(i);
            String table = si.getTable();
            String column = si.getColumn();
            columns[i] = table != null ? table+"."+column : column;
        }
        List<Schema> fromTables = getFromTables(query.getFrom());
        if(fromTables.size() == 1)
        {
            return database.from(fromTables.get(0).getName())
                    .select(columns);
        }
        return null;
    }
    
    private List<Schema> getFromTables(Vector<ZFromItem> from)
    {
        List<Schema> fromSchemas = new ArrayList<>();
        for (int i = 0; i < from.size(); i++) {
            ZFromItem fromIt = from.elementAt(i);
            Schema schema = database.getSchema(fromIt.getTable());
            if(schema == null)
                throw new DbException("SQLParser.getFromTables: " + fromIt.getTable() + " is not found");
            fromSchemas.add(schema);
        }
        return fromSchemas;
    }
    
}
