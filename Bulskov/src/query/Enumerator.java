package query;

import evaluation.LogicalOperation;
import evaluation.Predicate;
import net.palmund.query.HashJoinEnumerator;
import net.palmund.query.LeftOuterJoinEnumerator;
import net.palmund.query.MergeJoinEnumerator;
import storage.DbIterator;


/**
 *
 * @author bulskov
 */
public interface Enumerator extends DbIterator
{
    Enumerator select(String... columns);
    Enumerator where(LogicalOperation condition);
    SimpleJoinEnumerator join(Enumerator other, LogicalOperation condition);
    SimpleJoinEnumerator join(Enumerator other);

    LeftOuterJoinEnumerator leftJoin(Enumerator other, Predicate operation);
    HashJoinEnumerator hashJoin(Enumerator other, Predicate operation);

    MergeJoinEnumerator mergeJoin(Enumerator from, Predicate operation);
}
