package query;

import database.Schema;
import evaluation.LogicalOperation;
import evaluation.Predicate;
import net.palmund.query.HashJoinEnumerator;
import net.palmund.query.LeftOuterJoinEnumerator;
import net.palmund.query.MergeJoinEnumerator;
import storage.DbIterator;
import storage.Record;

/**
 *
 * @author bulskov
 */
public class FromEnumerator implements Enumerator
{
    DbIterator iterator;

    public FromEnumerator(DbIterator iterator)
    {
        this.iterator = iterator;
    }

    @Override
    public Enumerator select(String... columns)
    {
        return new SelectEnumerator(this, columns);
    }

    @Override
    public Enumerator where(LogicalOperation condition)
    {
        return new WhereEnnumerator(this, condition);
    }

    @Override
    public void open()
    {
        iterator.open();
    }

    @Override
    public boolean hasNext()
    {
        return iterator.hasNext();
    }

    @Override
    public Record next()
    {
        return iterator.next();
    }

    @Override
    public void rewind()
    {
        iterator.rewind();
    }

    @Override
    public void close()
    {
        iterator.close();
    }

    @Override
    public Schema getSchema()
    {
        return iterator.getSchema();
    }

    @Override
    public SimpleJoinEnumerator join(Enumerator other, LogicalOperation condition)
    {
        return new SimpleJoinEnumerator(this, other, condition);
    }

    @Override
    public SimpleJoinEnumerator join(Enumerator other) {
        return new SimpleJoinEnumerator(this, other);
    }

    @Override
    public LeftOuterJoinEnumerator leftJoin(Enumerator other, Predicate operation) {
        return new LeftOuterJoinEnumerator(this, other, operation);
    }

    @Override
    public HashJoinEnumerator hashJoin(Enumerator other, Predicate operation) {
        return new HashJoinEnumerator(this, other, operation);
    }

    @Override
    public MergeJoinEnumerator mergeJoin(Enumerator from, Predicate operation) {
        return new MergeJoinEnumerator(this, from, operation);
    }
}