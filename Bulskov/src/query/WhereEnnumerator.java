package query;

import evaluation.LogicalOperation;
import storage.Record;

/**
 *
 * @author bulskov
 */
public class WhereEnnumerator extends AbstractEnumerator
{
    private final LogicalOperation operation;
    private final boolean valid;

    public WhereEnnumerator(Enumerator source, LogicalOperation operation)
    {
        super(source);
        this.operation = operation;
        valid = operation.validate(getSchema());
    }
    
    @Override
    protected Record fetchNext()
    {
        while(valid && source.hasNext())
        {
            Record record = source.next();
            if(operation.evaluate(record, getSchema()))
                return record;
        }
        return null;
    }
}
