package query;

import database.DbException;
import database.Schema;
import storage.Record;

/**
 *
 * @author bulskov
 */
public class SelectEnumerator extends AbstractEnumerator
{
    private final int[] positions;
    private final Schema schema;
        
    public SelectEnumerator(Enumerator source, String[] columns)
    {
        super(source);
        positions = new int[columns.length];
        for(int i = 0; i < columns.length; i++)
        {
            positions[i] = source.getSchema().indexOf(columns[i]);
            if(positions[i] == -1)
            {
                throw new DbException("Projection.Projection: column '" + columns[i] + 
                        "' not found in relation '" + source.getSchema().getName() + "'");
            }
        }
        this.schema = source.getSchema().project(positions);
    }
    
    @Override
    protected Record fetchNext()
    {
        while(source.hasNext())
        {
            Record record = source.next();
            Record projection = new Record(schema);
            for(int i = 0; i < positions.length; i++)
                projection.setField(i, record.getField(positions[i]));
            return  projection;             
        }
        return null;
    }

    @Override
    public Schema getSchema()
    {
        return schema;
    }

}
