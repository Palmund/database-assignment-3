package query;

import database.DbException;
import database.Schema;
import evaluation.LogicalOperation;
import evaluation.Predicate;
import net.palmund.query.HashJoinEnumerator;
import net.palmund.query.LeftOuterJoinEnumerator;
import net.palmund.query.MergeJoinEnumerator;
import storage.Record;

/**
 *
 * @author bulskov
 */
public abstract class AbstractEnumerator implements Enumerator
{
    private Record next = null;
    protected final Enumerator source;
    
    public AbstractEnumerator(Enumerator source)
    {
        this.source = source;
        
    }
    
    @Override
    public Schema getSchema()
    {
        return source.getSchema();
    }
    
    @Override
    public void open()
    {
        source.open();
    }

    @Override
    public void rewind()
    {
        source.rewind();
    }

    @Override
    public void close()
    {
        source.close();
    }

    protected abstract Record fetchNext();

    @Override
    public Enumerator select(String... columns)
    {
        return new SelectEnumerator(this, columns);
    }
    
    @Override
    public Enumerator where(LogicalOperation condition)
    {
        return new WhereEnnumerator(this, condition);
    }
    
    @Override
    public SimpleJoinEnumerator join(Enumerator other, LogicalOperation condition)
    {
        return new SimpleJoinEnumerator(this, other, condition);
    }

    @Override
    public SimpleJoinEnumerator join(Enumerator other) {
        return new SimpleJoinEnumerator(this, other);
    }

    @Override
    public  Record next()
    {
        if (next == null) {
            next = fetchNext();
            if (next == null) 
                throw new DbException("Operator.next: no records");
        }

        Record result = next;
        next = null;
        return result;
    }

    /**
     * <b>NOTE:</b> This method has a side effect that loads the next element instead of simply replying whether it has next item.
     * @return
     */
    @Override
    public boolean hasNext()
    {
        if (next == null) 
            next = fetchNext();
        return next != null;
    }

    @Override
    public LeftOuterJoinEnumerator leftJoin(Enumerator other, Predicate operation) {
       return new LeftOuterJoinEnumerator(this, other, operation);
    }

    @Override
    public HashJoinEnumerator hashJoin(Enumerator other, Predicate operation) {
        return new HashJoinEnumerator(this, other, operation);
    }

    @Override
    public MergeJoinEnumerator mergeJoin(Enumerator from, Predicate operation) {
        return new MergeJoinEnumerator(this, from, operation);
    }
}
