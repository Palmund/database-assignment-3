
package database;

import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import storage.Block;
import storage.BlockId;
import storage.DbFile;
import storage.Record;

/**
 * This class handles the buffering in the database. You can set the number
 * of blocks in the database memory, and thus define the "size" of the 
 * database memory.
 * @author bulskov
 */
public class BufferManager {

    private final int numberOfBlocks;
    private int readCount = 0;
    private int totalCount = 0;

    private final Map<BlockId, Block> buffer = new HashMap<>();
    private final Deque<BlockId> recentUsed = new ArrayDeque<>();
    private final Database database;

    public BufferManager(int numberOfBlocks, Database database) {
        this.numberOfBlocks = numberOfBlocks;
        this.database = database;
    }

    private boolean isFull() {
        return buffer.size() >= numberOfBlocks;
    }

    private boolean inBuffer(BlockId blockId) {
        return buffer.containsKey(blockId);
    }

    // read a block from the buffer - mark it as pinned
    public Block pinBlock(BlockId blockId) throws IOException {
        totalCount++;
        Block block = null;
        if (!inBuffer(blockId)) {
            readCount++;
            if (isFull()) {
                evictBlock();
            }
            DbFile file = database.getFile(blockId.getSchemaId());
            block = file.readBlock(blockId);
            buffer.put(blockId, block);
            recentUsed.addFirst(blockId);
        }
        else
        {
            block = buffer.get(blockId);
        }
        block.incrementPinCount();
        return block;
    }
    
    // release the block
    public void unpinBlock(Block block) throws IOException {
        if(block.getPinCount() == 0)
            throw new DbException("BufferManager.unpinBlock: Block not pinned");
        block.decrementPinCount();
        if(block.getPinCount() == 0)
        {
            recentUsed.remove(block.getId());
            recentUsed.add(block.getId());
        }
    }

    public void insertRecord(final Schema schema, final Record record) throws IOException {
        DbFile file = database.getFile(schema.getId());
        file.insertRecord(this, record);
    }
//
//    public void delete(Schema schema, Record record) {
//        DbFile file = Database.getFile(schema.getId());
//        file.deleteRecord(record);
//    }

    // force all dirty block in the buffer to be written to disk
    public void flushAll() {
        for (BlockId blockId : buffer.keySet()) {
            flushBlock(blockId);
        }
    }

    private void flushBlock(BlockId blockId) {
        DbFile file = database.getFile(blockId.getSchemaId());
        Block block = buffer.get(blockId);
        if (block.isDirty()) {
            try {
                file.writeBlock(block);
            } catch (IOException ex) {
                throw new DbException("BufferManager.flushBlock: error writing block " + blockId);
            }
        }
    }

    // pick a block in the buffer to remove in order to make room for new
    // requests
    private void evictBlock() {
        final Iterator<BlockId> iterator = recentUsed.iterator();
        while(iterator.hasNext())
        {
            BlockId next = iterator.next();
            if(buffer.get(next).getPinCount() == 0)
            {
                iterator.remove();
                flushBlock(next);
                buffer.remove(next);
                return;
            }
        }
        // if all blocks in memory is in use - we ran out of memory
        throw new DbException("BufferManager.evictBlock: no free blocks");
    }

    public int getReadCount() {
        return readCount;
    }

    public int getTotalCount() {
        return totalCount;
    }
    
    
}
