package database;

import catalog.Attribute;
import catalog.DataType;
import catalog.Relation;
import query.FromEnumerator;
import storage.DbFile;
import storage.DbIterator;
import storage.FieldFactory;
import storage.Record;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The main access to the database
 * @author bulskov
 */
public final class Database {

    Map<String, Relation> relations = new HashMap<>();
    Map<Integer, DbFile> files = new HashMap<>();
    BufferManager manager;
        
    public Database(int bufferSize)
    {
        manager = new BufferManager(bufferSize, this);
        createCatalog();
        insertInto("sys_config", "buffer_size", Integer.toString(bufferSize));
    }
    
    public Database() 
    {
        manager = new BufferManager(20, this);
        loadCatalog();
        DbIterator sysConfigScanner = getTableScanner("sys_config");
        sysConfigScanner.open();
        int bufferSize = -1;
        while(sysConfigScanner.hasNext())
        {
            Record next = sysConfigScanner.next();
            if("buffer_size".equals((String)next.getData(0)))
            {
                bufferSize = Integer.parseInt((String)next.getData(1));
                break;
            }
        }
        if(bufferSize == -1)
            throw new DbException("Database.Database: error reading buffer_size from config");
        manager = new BufferManager(bufferSize, this);
        loadUserData();
    }

    public void createTable(String name, Attribute ... attributes)  {
        if (attributes.length == 0) {
            throw new IllegalArgumentException("Table must have at least one attribute");
        }
        Relation relation = new Relation(name);
        for (Attribute a : attributes) {
            relation.addAttribute(a);
        }
        try {
            createDatabaseFile(relation);
        } catch (IOException ex) {
            throw new DbException("Db.createTable: error creating database file");
        }
        saveRelation(relation);
    }

    private void createDatabaseFile(Relation relation) throws IOException {
        DbFile file = new DbFile(relation.getName()+".db", relation, true);
        files.put(relation.getId(), file);
    }
    
    public void dropTable(String name)
    {
        Relation relation = relations.remove(name);
        if(relation == null)
            throw new DbException("Db.dropTable: table '" + name + "' not found");
        DbFile file = files.remove(relation.getId());
        if(file != null)
        {
            try {
                file.close();
            } catch (IOException ex) {
                throw new DbException("Db.dropTable: error closing file");
            }
            file.getFile().delete();
        }
    }
    
    public Schema getSchema(String tableName)
    {
        return relations.get(tableName);
    }
    
    public DbFile getFile(String tableName)
    {
        Relation relation = relations.get(tableName);
        if(relation != null)
            return getFile(relation);
        return null;
    }
    
    public DbFile getFile(Relation relation)
    {
        return getFile(relation.getId());
    }
    
    public DbFile getFile(int id)
    {
        return files.get(id);
    }
    
    public BufferManager getBufferManager()
    {
        return manager;
    }
    
    public DbIterator getTableScanner(String tableName)
    {
        Relation relation = relations.get(tableName);
        if(relation == null)
            return null;
        return getTableScanner(relation);
    }
    
    public DbIterator getTableScanner(Relation relation)
    {
        DbFile file = files.get(relation.getId());
        if(file != null)
            return file.iterator(manager);
        return null;
    }

    public final void insertInto(String tableName, Object...data)
    {
        Relation relation = relations.get(tableName);
        if(relation == null)
            throw new DbException("Database.insertInto: relation '" + tableName + "' not found");
        Record rec = new Record(relation);
        for(int i = 0; i < data.length; i++)
        {
            rec.setField(i, FieldFactory.createField(data[i], relation.getAttribute(i)));
        }
        try
        {
            manager.insertRecord(relation, rec);
        } catch (IOException ex)
        {
            throw new DbException("Database.insertInto: " + ex.getMessage());
        }
    }
    
    public FromEnumerator from(String tableName)
    {
        Relation relation = relations.get(tableName);
        if(relation == null)
            throw new DbException("Database.select: relation '" + tableName + "' not found");
        DbFile file = files.get(relation.getId());
        return new FromEnumerator(file.iterator(manager));
    }

    public void commit()
    {
        manager.flushAll();
    }
    
    private void saveRelation(Relation relation)
    {
        if(relations.containsKey(relation.getName()))
            throw new DbException("Database.saveRelation: relation \""+relation.getName()+"\" exist");
        insertInto("sys_relation", relation.getName(), relation.getName()+".db");
        for(int i = 0; i < relation.numberOfAttributes(); i++)
        {
            Attribute attribute = relation.getAttribute(i);
            insertInto("sys_column", 
                            relation.getName(),
                            attribute.getName(),
                            attribute.getType().ordinal(),
                            i+1,
                            attribute.length());
        }
        relations.put(relation.getName(), relation);
    }

    private void createCatalog() 
    {
        Relation sysRelations = new Relation("sys_relation");
        sysRelations.addAttribute(Attribute.createString("table_name", 30));
        sysRelations.addAttribute(Attribute.createString("dbfile", 30));
        try
        {
            createDatabaseFile(sysRelations);
        } catch (IOException ex)
        {
            throw new DbException("Database.createCatalog: " + ex.getMessage());
        }
        relations.put(sysRelations.getName(), sysRelations);
        insertInto("sys_relation", "sys_relation", "sys_relation.db");
        
        Relation sysColumns = new Relation("sys_column");
        sysColumns.addAttribute(Attribute.createString("table_name", 30));
        sysColumns.addAttribute(Attribute.createString("column_name", 30));
        sysColumns.addAttribute(Attribute.createInteger("data_type"));
        sysColumns.addAttribute(Attribute.createInteger("position"));
        sysColumns.addAttribute(Attribute.createInteger("length"));
        try
        {
            createDatabaseFile(sysColumns);
        } catch (IOException ex)
        {
            throw new DbException("Database.createCatalog: " + ex.getMessage());
        }
        insertInto("sys_relation", "sys_column", "sys_column.db");
        relations.put(sysColumns.getName(), sysColumns);
        
        insertInto("sys_column", "sys_relation", "table_name", DataType.STRING.ordinal(), 1, 30);
        insertInto("sys_column", "sys_relation", "dbfile", DataType.STRING.ordinal(), 2, 30);
        
        insertInto("sys_column", "sys_column", "table_name", DataType.STRING.ordinal(), 1, 30);
        insertInto("sys_column", "sys_column", "column_name", DataType.STRING.ordinal(), 2, 30);
        insertInto("sys_column", "sys_column", "data_type", DataType.INTEGER.ordinal(), 3, 0);
        insertInto("sys_column", "sys_column", "position", DataType.INTEGER.ordinal(), 4, 0);
        insertInto("sys_column", "sys_column", "length", DataType.INTEGER.ordinal(), 5, 0);
        
        
        Relation sysConfig = new Relation("sys_config");
        sysConfig.addAttribute(Attribute.createString("key", 30));
        sysConfig.addAttribute(Attribute.createString("value", 30));
        try
        {
            createDatabaseFile(sysConfig);
        } catch (IOException ex)
        {
            throw new DbException("Database.createCatalog: " + ex.getMessage());
        }
        saveRelation(sysConfig);
        
    }

    private void loadCatalog()
    {
        File sysConfig = new File("sys_config.db");
        if(!sysConfig.exists())
            throw new DbException("Database.loadCatalog: no sys_config file");
        File sysRelation = new File("sys_relation.db");
        if(!sysRelation.exists())
            throw new DbException("Database.loadCatalog: no sys_relation file");
        File sysColumn = new File("sys_column.db");
        if(!sysColumn.exists())
            throw new DbException("Database.loadCatalog: no sys_column file");
        
        Relation sysConfigSchema = new Relation("sys_config");
        sysConfigSchema.addAttribute(Attribute.createString("key", 30));
        sysConfigSchema.addAttribute(Attribute.createString("value", 30));
        try
        {
            DbFile dbFile = new DbFile("sys_config.db", sysConfigSchema, false);
            files.put(sysConfigSchema.getId(), dbFile);
            relations.put(sysConfigSchema.getName(), sysConfigSchema);
        } catch (IOException ex)
        {
            throw new DbException("Database.loadCatalog: no sys_config file");
        }
        
        Relation sysRelationSchema = new Relation("sys_relation");
        sysRelationSchema.addAttribute(Attribute.createString("table_name", 30));
        sysRelationSchema.addAttribute(Attribute.createString("dbfile", 30));
        try
        {
            DbFile dbFile = new DbFile("sys_relation.db", sysRelationSchema, false);
            files.put(sysRelationSchema.getId(), dbFile);
            relations.put(sysRelationSchema.getName(), sysRelationSchema);
        } catch (IOException ex)
        {
            throw new DbException("Database.loadCatalog: no sys_relation file");
        }
             
        Relation sysColumnSchema = new Relation("sys_column");
        sysColumnSchema.addAttribute(Attribute.createString("table_name", 30));
        sysColumnSchema.addAttribute(Attribute.createString("column_name", 30));
        sysColumnSchema.addAttribute(Attribute.createInteger("data_type"));
        sysColumnSchema.addAttribute(Attribute.createInteger("position"));
        sysColumnSchema.addAttribute(Attribute.createInteger("length"));
        try
        {
            DbFile dbFile = new DbFile("sys_column.db", sysColumnSchema, false);
            files.put(sysColumnSchema.getId(), dbFile);
            relations.put(sysColumnSchema.getName(), sysColumnSchema);
        } catch (IOException ex)
        {
            throw new DbException("Database.loadCatalog: no sys_column file");
        }
    }

    private void loadRelation(String tableName, String fileName)
    {
        Relation schema = loadSchema(tableName);
        try
        {
            DbFile file = new DbFile(fileName, schema, false);
            files.put(schema.getId(), file);
        relations.put(tableName, schema);
        } catch (IOException ex)
        {
            throw new DbException("Database.loadRelation: " + ex.getMessage());
        }
        
    }

    private void loadUserData()
    {
        DbIterator tableScanner = getTableScanner("sys_relation");
        tableScanner.open();
        while(tableScanner.hasNext())
        {
            Record r = tableScanner.next();
            if(!((String)r.getData(0)).startsWith("sys_"))
            {
                loadRelation((String)r.getData(0), (String)r.getData(1));
            }
        }
    }

    private Relation loadSchema(String tableName)
    {
        Relation relation = new Relation(tableName);
        DbIterator scanner = getTableScanner("sys_column");
        scanner.open();
        Map<Integer, Attribute> attrMap = new HashMap<>();
        while(scanner.hasNext())
        {
            Record r = scanner.next();
            //System.out.println(r);
            if(tableName.equalsIgnoreCase((String)r.getData(0)))
            {
                DataType type = DataType.values()[((Integer)r.getData(2)).intValue()];
                switch(type)
                {
                    case INTEGER:
                        attrMap.put((Integer)r.getData(3), Attribute.createInteger((String)r.getData(1)));
                        break;
                    case FLOAT:
                        attrMap.put((Integer)r.getData(3), Attribute.createFloat((String)r.getData(1)));
                        break;
                    case STRING:
                        attrMap.put((Integer)r.getData(3), Attribute.createString((String)r.getData(1), (Integer)r.getData(4)));
                        break;
                    default:
                        throw new DbException("Database.loadSchema: Unknown datatype");
                }
            }
        }
        for(int i = 0; i < attrMap.size(); i++)
            relation.addAttribute(attrMap.get(i+1));
        return relation;
    }
    
    public void close()
    {
        commit();
        for(DbFile file : files.values())
        {
            try
            {
                file.close();
            } catch (IOException ex)
            {
                Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    
    
}
