package database;

import catalog.Attribute;

/**
 *
 * @author bulskov
 */
public interface Schema {

    void addAttribute(Attribute attribute);

    Attribute getAttribute(final int index);

    int numberOfAttributes();

    // return the size to a record with this schema
    int size();
    
    String getName();
    
    int getId();
    
    Schema merge(Schema otherSchema);

    public int indexOf(String string);
    
    public Schema project(int[] positions);
    
    boolean contains(Attribute attribute);
}
