package database;

/**
 *
 * @author bulskov
 */
public class DbException extends RuntimeException {

    public DbException(String message) {
        super(message);
    }
    
}
