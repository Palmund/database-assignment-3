package catalog;

import database.DbException;
import database.Schema;
import java.util.ArrayList;
import java.util.List;

/**
 * This class is the schema for a table in he database.
 * @author bulskov
 */
public class Relation implements Schema {
    private final String name;
    private List<Attribute> attributes;
    private RelationType type;
    
    public Relation(String name)
    {
        this.name = name;
    }

    @Override
    public void addAttribute(Attribute attribute) {
        if(attributes == null)
            attributes = new ArrayList<>();
        attributes.add(attribute);
    }

    @Override
    public Attribute getAttribute(int index) {
        return attributes.get(index);
    }

    @Override
    public int numberOfAttributes() {
        return attributes.size();
    }

    @Override
    public int size() {
        int size = 0; 
        for(Attribute attribute : attributes)
            size += attribute.size();
        return size;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getId() {
        return name.hashCode();
    }

    @Override
    public int indexOf(String columnName)
    {
        for(int i = 0; i < attributes.size(); i++)
        {
            if(attributes.get(i).getName().equalsIgnoreCase(columnName))
                return i;
        }
        return -1;
    }
    
    @Override
    public boolean contains(Attribute attribute)
    {
        return attributes.contains(attribute);
    }

    @Override
    public Schema merge(Schema otherSchema)
    {
        if(!(otherSchema instanceof Relation))
        {
            throw new DbException("Schema.merge: not same schema type");
        }
        Schema mergedSchema = new Relation(name + "_" + otherSchema);
        for(Attribute attribute : attributes)
        {
            Attribute a;
            if(otherSchema.contains(attribute))
                a = Attribute.create(name+"."+attribute.getName(), attribute.getType(), attribute.getLength());
            else
                a = Attribute.create(attribute);
            mergedSchema.addAttribute(a);
        }
        for(Attribute attribute : ((Relation)otherSchema).attributes)
        {
            Attribute a;
            if(this.contains(attribute))
                a = Attribute.create(otherSchema.getName()+"."+attribute.getName(), attribute.getType(), attribute.getLength());
            else
                a = Attribute.create(attribute);
            mergedSchema.addAttribute(a);
        }
        return mergedSchema;
    }
    
    public Schema project(int[] positions)
    {
        if(positions.length < 1)
            throw new DbException("Schema.project: a projection must at least have one column");
        Schema projectedSchema = new Relation(name + "_projection");
        for(int pos : positions)
            projectedSchema.addAttribute(attributes.get(pos));
        return projectedSchema;
    }
    
    public Schema project(String...columns)
    {
        if(columns.length < 1)
            throw new DbException("Schema.project: a projection must at least have one column");
        Schema projectedSchema = new Relation(name + "_projection");
        for(Attribute attribute : attributes)
        {
            for(String name : columns)
            {
                if(name.equalsIgnoreCase(attribute.getName()))
                    projectedSchema.addAttribute(attribute);
            }
        }
        return projectedSchema;
    }
}
