package catalog;

/**
 *
 * @author bulskov
 */
public enum RelationType {
    SYSTEM, USER
}
