package catalog;

/**
 *
 * @author bulskov
 */
public enum DataType {
    
    INTEGER() {
        @Override
        public int size()
        {
            return Integer.SIZE / Byte.SIZE;
        }
    },
    FLOAT() {
        @Override
        public int size()
        {
            return Float.SIZE / Byte.SIZE;
        }
    },           
     STRING() {
        @Override
        public int size()
        {
            return Integer.SIZE / Byte.SIZE;
        }
    },
     // not save to disk, but used in queries
    COLNAME() {
        @Override
        public int size()
        {
            return Integer.SIZE / Byte.SIZE;
        }
    }, 
    // not save to disk, but used in queries
    FIELDNO() {
        @Override
        public int size()
        {
            return Integer.SIZE / Byte.SIZE;
        }
    };
    
    // return the size in bytes of the type
    public abstract int size();

}
