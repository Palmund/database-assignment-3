package catalog;

import java.util.Objects;

/**
 * This class hold information about an attribute in a schema. That is
 * the name, the data type, and the length (length is only used for strings)
 * @author bulskov
 */
public class Attribute {
    private String name;
    private DataType type;
    private int length;

    public String getName() {
        return name;
    }

    public DataType getType() {
        return type;
    }

    public int getLength() {
        return length;
    }
    
    private Attribute(final String name, final DataType type)
    {
        this(name, type, 0);
    }
    
    private Attribute(final String name, final DataType type, final int length)
    {
        this.name = name;
        this.type = type;
        this.length = length;
        
    }
    
    public static Attribute createInteger(final String name)
    {
        return new Attribute(name, DataType.INTEGER);
    }
    
    public static Attribute createFloat(final String name)
    {
        return new Attribute(name, DataType.FLOAT);
    }
    
    public static Attribute createString(final String name, int length)
    {
        return new Attribute(name, DataType.STRING, length);
    }
    
    public static Attribute create(final String name, DataType type, int length)
    {
        return new Attribute(name, type, length);
    }
    
    public static Attribute create(Attribute attribute)
    {
        return new Attribute(attribute.name, attribute.type, attribute.length);
    }
    
    public int length()
    {
        return length;
    }

    // this is used to define size on disk
    public int size()
    {
        return length + type.size();
    }

    @Override
    public int hashCode()
    {
        return name.hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final Attribute other = (Attribute) obj;
        if (!Objects.equals(this.name, other.name))
        {
            return false;
        }
        return true;
    }
}
