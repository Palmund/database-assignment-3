package evaluation;

import database.Schema;
import storage.Field;
import storage.Record;

/**
 *
 * @author bulskov
 */
public class SortKeyExtractor
{

    private final Schema schema;
    private final int[] keyFieldNos;

    public SortKeyExtractor(final Schema schema, final Integer... keyFieldNos)
    {
        this.schema = schema;
        this.keyFieldNos = new int[keyFieldNos.length];
        for (int i = 0; i < keyFieldNos.length; i++)
        {
            final int keyFieldNo = keyFieldNos[i].intValue();
            if (keyFieldNo < schema.numberOfAttributes())
            {
                this.keyFieldNos[i] = keyFieldNo;
            } else
            {
                throw new IllegalArgumentException("There is no field with number " + keyFieldNo
                        + "in the schema");
            }
        }
    }
    
    public SortKey extract(Record record)
    {
        
        final Field[] fields = new Field[this.keyFieldNos.length];
        for (int i = 0; i < keyFieldNos.length; i++) {
            fields[i] = record.getField(keyFieldNos[i]);
        }
        return new SortKey(fields);
    }
}
