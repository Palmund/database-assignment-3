package evaluation;

import catalog.DataType;
import database.Schema;
import storage.Record;

/**
 *
 * @author bulskov
 */
public class Predicate implements LogicalOperation
{

    protected Operator operator;
    protected DataType leftType;
    protected DataType rightType;
    protected Object left;
    protected Object right;

    public Predicate(Operator operator, DataType leftType, Object left, DataType rightType, Object right)
    {
        this.operator = operator;
        this.leftType = leftType;
        this.left = left;
        this.rightType = rightType;
        this.right = right;
    }
    
    public static LogicalOperation compareFieldField(Operator operator, int leftFieldNo, int rightFieldNo)
    {
        return new Predicate(operator, DataType.FIELDNO, leftFieldNo, DataType.FIELDNO, rightFieldNo);
    }
    
    public static LogicalOperation compareColumnColumn(Operator operator, String leftColumn, String rightColumn)
    {
        return new Predicate(operator, DataType.COLNAME, leftColumn, DataType.COLNAME, rightColumn);
    }
    
    public static LogicalOperation compareColumnValue(Operator operator, String columnName, DataType valueType, Object value)
    {
        return new Predicate(operator, DataType.COLNAME, columnName, valueType, value);
    }
    
    public static LogicalOperation compareFieldValue(Operator operator, int columnNo, DataType valueType, Object value)
    {
        return new Predicate(operator, DataType.FIELDNO, columnNo, valueType, value);
    }

    @Override
    public boolean validate(Schema schema)
    {
        DataType type1 = leftType;
        if (type1 == DataType.COLNAME)
        {
            int pos = schema.indexOf((String) left);
            if (pos < 0)
            {
                return false;
            }
            type1 = schema.getAttribute(pos).getType();
        }
        DataType type2 = rightType;
        if (type2 == DataType.COLNAME)
        {
            int pos = schema.indexOf((String) right);
            if (pos < 0)
            {
                return false;
            }
            type2 = schema.getAttribute(pos).getType();
        }
        return (type1 == type2);
    }

    @Override
    public boolean evaluate(Record record, Schema schema)
    {
        if (leftType == DataType.COLNAME)
        {
            left = schema.indexOf((String) left);
            leftType = DataType.FIELDNO;
        }
        if (rightType == DataType.COLNAME)
        {
            right = schema.indexOf((String) right);
            rightType = DataType.FIELDNO;
        }

        DataType type = leftType;
        Object leftValue = left;
        if (leftType == DataType.FIELDNO)
        {
            type = schema.getAttribute((Integer) leftValue).getType();
            leftValue = record.getData((Integer) leftValue);
        }
        Object rightValue = right;
        if (rightType == DataType.FIELDNO)
        {
            rightValue = record.getData((Integer) rightValue);
        }
        int comp = 0;
        switch (type)
        {
            case INTEGER:
                comp = ((Integer) leftValue).compareTo((Integer) rightValue);
                break;
            case FLOAT:
                comp = ((Float) leftValue).compareTo((Float) rightValue);
                break;
            case STRING:
                comp = ((String) leftValue).compareTo((String) rightValue);
                break;
            default:
                throw new IllegalStateException("unknown types to compare");
        }

        switch (operator)
        {
            case EQ: return (comp == 0);
            case NEQ: return (comp != 0);
            case GT: return (comp > 0);
            case GTE: return (comp >= 0);
            case LT: return (comp < 0);
            case LTE: return (comp <= 0);
            default: throw new IllegalStateException("unknown operator to evaluate");
        }
    }

    public Object getLeft() {
        return left;
    }

    public Object getRight() {
        return right;
    }
}
