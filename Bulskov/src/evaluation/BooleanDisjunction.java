package evaluation;

import database.Schema;
import storage.Record;

/**
 *
 * @author bulskov
 */
public class BooleanDisjunction implements LogicalOperation
{
    LogicalOperation left;
    LogicalOperation right;
    
    public BooleanDisjunction(LogicalOperation left, LogicalOperation right)
    {
        this.left = left;
        this.right = right;
    }
    
    @Override
    public boolean validate(Schema schema)
    {
        return left.validate(schema) && right.validate(schema);
    }

    @Override
    public boolean evaluate(Record record, Schema schema)
    {
        return left.evaluate(record, schema) || right.evaluate(record, schema);
    }
}
