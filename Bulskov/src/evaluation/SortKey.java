package evaluation;

import catalog.Attribute;
import storage.Field;

/**
 *
 * @author bulskov
 */
public class SortKey implements Comparable<SortKey>
{
    private final Field[] fields;

   protected SortKey(final Field... fields) {
      this.fields = fields;
   }
   
   protected Field getKeyField(final int fieldNo) {
      return this.fields[fieldNo];
   }
   
   protected int size() {
      return this.fields.length;
   }
   
   @Override
    public int compareTo(SortKey sortKey) {
        if(sortKey.fields.length != fields.length)
            throw new IllegalArgumentException();
        for(int i = 0; i < sortKey.size(); i++)
        {
            int compareTo = fields[i].compareTo(sortKey.fields[i]);
            if(compareTo != 0 || i == sortKey.size() - 1)
                return compareTo;
        }
        throw new IllegalArgumentException();
    }
}
