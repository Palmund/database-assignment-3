package evaluation;

import database.Schema;
import storage.Record;

/**
 *
 * @author bulskov
 */
public interface LogicalOperation
{
    boolean validate(Schema schema);
    boolean evaluate(Record record, Schema schema);
}
