package evaluation;

import catalog.DataType;

/**
 * This class is intended to provide a way to be more declarative when issuing conditions on the {@link database.Database}.<br/>
 *
 * Example:
 *
 * <pre>Database.from("someTable")
 * .where(column("column1").isEqualTo(column("column2"));
 * </pre>
 *
 * Created by Søren Palmund
 */
public final class Operations {
    public static FieldOperation field(int fieldNumber) {
        return new FieldOperation(fieldNumber);
    }

    public static ColumnOperation column(String column) {
        return new ColumnOperation(column);
    }

    public static Type string(String value) {
        return new Type<>(DataType.STRING, value);
    }

    public static Type integer(int value) {
        return new Type<>(DataType.INTEGER, value);
    }

    public static Type floating(float value) {
        return new Type<>(DataType.STRING, value);
    }

    public static class FieldOperation extends Type<Integer> {
        public FieldOperation(int fieldNumber) {
            super(DataType.FIELDNO, fieldNumber);
        }
    }

    public static class ColumnOperation extends Type<String> {
        public ColumnOperation(String column) {
            super(DataType.COLNAME, column);
        }
    }

    private static class Type<T> {
        private final DataType type;
        private final T value;

        private Type(DataType type, T value) {
            this.type = type;
            this.value = value;
        }

        public Predicate isEqualTo(Type anotherType) {
            return new Predicate(Operator.EQ, this.type, this.value, anotherType.type, anotherType.value);
        }

        public LogicalOperation isNotEqualTo(Type value) {
            return new Predicate(Operator.NEQ, this.type, this.value, value.type, value.value);
        }

        public LogicalOperation isGreaterThan(Type value) {
            return new Predicate(Operator.GT, this.type, this.value, value.type, value.value);
        }

        public LogicalOperation isLessThan(Type value) {
            return new Predicate(Operator.LT, this.type, this.value, value.type, value.value);
        }

        public LogicalOperation isLessThanOrEqualTo(Type value) {
            return new Predicate(Operator.LTE, this.type, this.value, value.type, value.value);
        }

        public LogicalOperation isGreaterThanOrEqualTo(Type value) {
            return new Predicate(Operator.GT, this.type, this.value, value.type, value.value);
        }
    }
}