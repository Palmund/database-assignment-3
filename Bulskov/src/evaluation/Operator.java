package evaluation;


/**
 * @author bulskov
 */
public enum Operator {

    EQ(11, "=") {
        @Override
        public boolean evaluate(int comp) {
            return (comp == 0);
        }
    },
    NEQ(12, "<>") {
        @Override
        public boolean evaluate(int comp) {
            return (comp != 0);
        }
    },
    GT(13, ">") {
        @Override
        public boolean evaluate(int comp) {
            return (comp > 0);
        }
    },
    GTE(14, ">=") {
        @Override
        public boolean evaluate(int comp) {
            return (comp >= 0);
        }
    },
    LT(15, "<") {
        @Override
        public boolean evaluate(int comp) {
            return (comp < 0);
        }
    },
    LTE(16, "<=") {
        @Override
        public boolean evaluate(int comp) {
            return (comp <= 0);
        }
    };

    private final int operatorId;
    private final String literal;

    private Operator(final int operatorId, String literal) {
        this.operatorId = operatorId;
        this.literal = literal;
    }

    public static Operator getOperator(String str) {
        final Operator[] values = Operator.values();
        for (Operator value : values) {
            if (value.literal.equals(str)) {
                return value;
            }
        }
        throw new IllegalArgumentException("unknown operator");
    }

    public static Operator getInstance(final int operatorId) {
        final Operator[] values = Operator.values();
        for (Operator value : values) {
            if (value.operatorId == operatorId) {
                return value;
            }
        }
        throw new IllegalArgumentException("Unknown operator " + operatorId);
    }

    @Override
    public String toString() {
        return literal;
    }

    public int getOperatoId() {
        return this.operatorId;
    }

    public abstract boolean evaluate(int comp);
}