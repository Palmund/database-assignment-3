package evaluation;

import database.Schema;
import storage.Record;

/**
 *
 * @author bulskov
 */
public class BooleanNegation implements LogicalOperation
{
    LogicalOperation operation;

    public BooleanNegation(LogicalOperation operation)
    {
        this.operation = operation;
    }
    
    @Override
    public boolean validate(Schema schema)
    {
        return operation.validate(schema);
    }

    @Override
    public boolean evaluate(Record record, Schema schema)
    {
        return !operation.evaluate(record, schema);
    }
    
}
