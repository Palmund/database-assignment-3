package storage;

import database.Schema;

/**
 *
 * @author bulskov
 */
public interface DbIterator {

    public void open();

    public boolean hasNext();

    public Record next();

    public void rewind();
    
    public void close();
    
    public Schema getSchema();
}
