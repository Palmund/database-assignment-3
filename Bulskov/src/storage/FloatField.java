package storage;

import catalog.DataType;
import database.DbException;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 *
 * @author bulskov
 */
public class FloatField implements Field {
    private float value;
    private DataType type;
    
    public FloatField(float value)
    {
        this.value = value;
        type = DataType.FLOAT;
    }
    
    public FloatField(DataInputStream dis) throws IOException
    {
        this(dis.readFloat());
    }
    
    @Override
    public void write(DataOutputStream dos) throws IOException {
        dos.writeFloat(value);
    }

    @Override
    public DataType getType() {
        return type;
    }

    @Override
    public int hashCode() {
        return ((Float)value).hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FloatField other = (FloatField) obj;
        if (this.value != other.value) {
            return false;
        }
        return true;
    }

    @Override
    public Object getValue()
    {
        return value;
    }

    @Override
    public int compareTo(Field field)
    {
        switch(field.getType())
        {
            case FLOAT:
                return ((Float)value).compareTo((Float)field.getValue());
            default:
                throw new DbException("FloatField.compareTo: not comparable types " +
                        type.name() + "compareTo(" + field.getType().name() + ")");
        }
    }
}
