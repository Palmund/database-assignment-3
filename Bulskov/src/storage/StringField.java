package storage;

import catalog.DataType;
import database.DbException;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 *
 * @author bulskov
 */
public class StringField implements Field
{

    String value;
    int maxLength;
    DataType type;

    public StringField(String value, int maxLength)
    {
        this.value = value;
        this.maxLength = maxLength;
        type = DataType.STRING;
    }
    
    public StringField(DataInputStream dis, int maxLength) throws IOException
    {
        int strLen = dis.readInt();
        byte bs[] = new byte[strLen];
        dis.read(bs);
        dis.skipBytes(maxLength - strLen);
        this.value = new String(bs);
        this.maxLength = maxLength;
        type = DataType.STRING;
    }

    @Override
    public void write(DataOutputStream dos) throws IOException
    {
        String s = value;
        int overflow = maxLength - s.length();
        if (overflow < 0) // clip
        {
            s = s.substring(0, maxLength);
        }
        dos.writeInt(s.length());
        dos.writeBytes(s);
        while (overflow-- > 0)
        {
            dos.write((byte) 0);
        }
    }

    @Override
    public DataType getType()
    {
        return type;
    }

    @Override
    public int hashCode()
    {
        return value.hashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final StringField other = (StringField) obj;
        if (!this.value.equals(other.value))
        {
            return false;
        }
        return true;
    }

    @Override
    public Object getValue()
    {
        return value;
    }
    
    @Override
    public int compareTo(Field field)
    {
        switch(field.getType())
        {
            case INTEGER:
                return value.toString().compareTo(((Integer)field.getValue()).toString());
            case FLOAT:
                return value.toString().compareTo(((Float)field.getValue()).toString());
            case STRING:
                return value.toString().compareTo(field.getValue().toString());
            default:
                throw new DbException("StringField.compareTo: not comparable types " +
                        type.name() + "compareTo(" + field.getType().name() + ")");
        }
    }
}
