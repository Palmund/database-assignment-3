package storage;

import database.BufferManager;
import database.Schema;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author bulskov
 */
public class DbFileIterator implements DbIterator {
    private final DbFile file;
    private int currentBlockId;
    private Block currentBlock;
    private BlockIterator blockIterator;
    private final BufferManager bufferManager;
    
    
    public DbFileIterator(DbFile file, BufferManager bufferManager)
    {
        this.file = file;
        currentBlockId = DbFile.FIRST_DATA_BLOCK;
        this.bufferManager = bufferManager;
    }

    @Override
    public void open() {
        currentBlock = readBlock(currentBlockId++);
        if(currentBlock != null)
            blockIterator = currentBlock.iterator();
    }

    @Override
    public boolean hasNext() {
        if(blockIterator == null)
            return false;
        if(blockIterator.hasNext())
            return true;
        
        while(currentBlockId <= file.getNumberOfBlocks())
        {
            currentBlock = readBlock(currentBlockId++);
            blockIterator = currentBlock.iterator();
            if(blockIterator.hasNext())
                return true;
        }
        return false;        
    }

    @Override
    public Record next() {
        if(blockIterator == null)
            throw new IllegalArgumentException("DbFileIterator.next: iterator not open");
        return blockIterator.next();
    }

    @Override
    public void rewind() {
        close();
        open();
    }

    private Block readBlock(int id) {
        try {
            if(currentBlock != null)
                bufferManager.unpinBlock(currentBlock);
            BlockId blockId = new BlockId(file.getSchema().getId(), id);
            return bufferManager.pinBlock(blockId);
        } catch (IOException ex) {
            Logger.getLogger(DbFileIterator.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public void close()
    {
        currentBlockId = DbFile.FIRST_DATA_BLOCK;
        blockIterator = null;
    }

    @Override
    public Schema getSchema()
    {
        return file.getSchema();
    }
    
    
    
}
