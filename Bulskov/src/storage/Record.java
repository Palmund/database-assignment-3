package storage;

import database.Schema;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 *
 * @author bulskov
 */
public class Record
{

    private RecordId recordId;
    private final Schema schema;
    private final Field[] fields;

    public Record(Schema schema)
    {
        this.schema = schema;
        fields = new Field[schema.numberOfAttributes()];
    }


    public Record(Schema schema, DataInputStream dis) throws IOException
    {
        recordId = new RecordId(dis);
        this.schema = schema;
        int numberOfAttributes = schema.numberOfAttributes();
        fields = new Field[numberOfAttributes];
        for (int i = 0; i < numberOfAttributes; i++)
        {
            setField(i, FieldFactory.createField(dis, schema.getAttribute(i)));
            
        }
    }

    public Record padAccordingToSchema(Schema schema) {
        Record joinRecord = new Record(schema);
        int i = 0;
        while (i < fields.length) {
            joinRecord.setField(i, fields[i]);
            i++;
        }
        while (i < schema.numberOfAttributes()) {
            joinRecord.setField(i, new StringField("null", 4));
            i++;
        }
        return joinRecord;
    }

    public final void setField(int index, Field field)
    {
        fields[index] = field;
    }

    public Field getField(int index)
    {
        return fields[index];
    }

    public RecordId getRecordId()
    {
        return recordId;
    }

    public void setRecordId(RecordId recordId)
    {
        this.recordId = recordId;
    }

    public int size()
    {
        return schema.size() + RecordId.SIZE;
    }

    public void write(DataOutputStream dos) throws IOException
    {
        recordId.write(dos);
        for (int i = 0; i < schema.numberOfAttributes(); i++)
        {
            fields[i].write(dos);
        }
    }

    public Object getData(final int index)
    {
        return fields[index].getValue();
    }

    @Override
    public String toString()
    {

        StringBuilder sb = new StringBuilder("[");
        for (int i = 0; i < fields.length; i++)
        {
            if (i > 0)
            {
                sb.append(", ");
            }
            sb.append(fields[i].getValue().toString());

        }
        sb.append("]");
        return sb.toString();
    }

    /**
     * Returns an instance of Record that is a join of {@code schema}.
     *
     * @param record
     * @param schema
     * @return
     */
    public Record join(Record record, Schema schema)
    {
        Record joinRecord = new Record(schema);
        int pos = 0;
        for(Field field : this.fields) {
            joinRecord.setField(pos++, field);
        }
        for(Field field : record.fields) {
            joinRecord.setField(pos++, field);
        }
        return joinRecord;
    }

    public Schema getSchema() {
        return schema;
    }
}
