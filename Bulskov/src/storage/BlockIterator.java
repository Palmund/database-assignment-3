package storage;

import database.Schema;

/**
 *
 * @author bulskov
 */
public class BlockIterator implements DbIterator {
    private final Block block;
    private int currentSlot = 0;
    
    public BlockIterator(Block block)
    {
        this.block = block;
    }

    @Override
    public void open() {
        currentSlot = 0;
    }

    @Override
    public boolean hasNext() {
        if(block == null)
            return false;
        while(currentSlot < block.getNumberOfRecords())
        {
            if(block.isValid(currentSlot))
                return true;
            currentSlot++;
        }
        return false;
    }

    @Override
    public Record next() {
        return block.getRecord(currentSlot++);
    }

    @Override
    public void rewind() {
        currentSlot = 0;
    }

    @Override
    public void close() {
        
    }

    @Override
    public Schema getSchema()
    {
        throw new UnsupportedOperationException("Not supported.");
    }
    
}
