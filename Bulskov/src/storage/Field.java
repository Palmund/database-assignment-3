package storage;

import catalog.DataType;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 *
 * @author bulskov
 */
public interface Field extends Comparable<Field>{
    void write(DataOutputStream dos) throws IOException;
    DataType getType();
    Object getValue();
}
