package storage;

import catalog.Attribute;
import database.DbException;
import java.io.DataInputStream;
import java.io.IOException;

/**
 *
 * @author bulskov
 */
public class FieldFactory
{

    public static Field createField(DataInputStream dis, Attribute attribute) throws IOException
    {
        switch (attribute.getType())
        {
            case INTEGER:
                return new IntegerField(dis.readInt());
            case FLOAT:
                return new FloatField(dis.readFloat());
            case STRING:
                int strLen = dis.readInt();
                byte bs[] = new byte[strLen];
                dis.read(bs);
                dis.skipBytes(attribute.getLength() - strLen);
                return new StringField(new String(bs), attribute.getLength());
            default:
                throw new DbException("FieldFactory.createField: Unknown data type");
        }
    }
    
    public static Field createField(Object value, Attribute attribute) 
    {
        switch (attribute.getType())
        {
            case INTEGER:
                return new IntegerField((Integer)value);
            case FLOAT:
                return new FloatField((Float)value);
            case STRING:
                return new StringField((String)value, attribute.getLength());
            default:
                throw new DbException("FieldFactory.createField: Unknown data type");
        }
    }
}
