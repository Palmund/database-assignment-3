package storage;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Objects;

/**
 *
 * @author bulskov
 */
public class RecordId {
    public static final int SIZE = (Integer.SIZE/Byte.SIZE) + BlockId.SIZE;
    private final BlockId blockId;
    private final int slot;
    
    public RecordId(BlockId blockId, int slot)
    {
        this.blockId = blockId;
        this.slot = slot;
    }
    
    public RecordId(DataInputStream dis) throws IOException
    {
        blockId = new BlockId(dis);
        slot = dis.readInt();
    }

    public BlockId getBlockId() {
        return blockId;
    }

    public int getSlot() {
        return slot;
    }
    
    public void write(DataOutputStream dos) throws IOException
    {
        blockId.write(dos);
        dos.writeInt(slot);
    }

    @Override
    public int hashCode() {
        int mask = 0x0000FFFF;
        int hash = (slot & mask) << 16;
        hash = hash | (blockId.hashCode() & mask);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RecordId other = (RecordId) obj;
        if (!Objects.equals(this.blockId, other.blockId)) {
            return false;
        }
        if (this.slot != other.slot) {
            return false;
        }
        return true;
    }
}
