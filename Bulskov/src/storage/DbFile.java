package storage;

import database.BufferManager;
import database.DbException;
import database.Schema;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 *
 * @author bulskov
 */
public class DbFile {
    public static final int FIRST_DATA_BLOCK = 1;
    private File file;
    private Schema schema;
    private FileChannel fileChannel;
    private int firstFreeBlock = -1;
    private int numberOfBlocks = 0;
    //private ;

    
    public DbFile(String filename, Schema schema, boolean newFile) throws FileNotFoundException, IOException
    {
        this.schema = schema;
        file = new File(filename);
        if (newFile && file.exists()) {
            file.delete();
        }
        RandomAccessFile randomAccessFile;
        randomAccessFile = new RandomAccessFile(file, "rw");
        fileChannel = randomAccessFile.getChannel();
        if(newFile)
            saveHeader();
        else
            readHeader();        
    }

    public int getNumberOfBlocks() {
        return numberOfBlocks;
    }

    public Block readBlock(final BlockId blockId) throws IOException {
        return readBlock(blockId.getBlockId());
    }
    
    private Block readBlock(int blockId) throws IOException
    {
        int offset = blockId * Block.BLOCK_SIZE;
        ByteBuffer buffer = ByteBuffer.allocate(Block.BLOCK_SIZE);
        fileChannel.read(buffer, offset);
        return new Block(buffer.array(), schema);
    }

    public void writeBlock(Block block) throws IOException {
        BlockId blockId = block.getId();
        int offset = blockId.getBlockId() * Block.BLOCK_SIZE;
        
        // handle freespace
        if(block.hasFreeSpace())
        {
            if(block.getNextBlock() == -1 && block.getPrevBlock() == -1)
            {
                block.setNextBlock(firstFreeBlock);
                firstFreeBlock = block.getId().getBlockId();
            }
        }
        else  
        {
            // first free block?
            if(firstFreeBlock == block.getId().getBlockId())
            {
                firstFreeBlock = block.getNextBlock();
                block.setNextBlock(-1);
            }
            // last
            else if(block.getPrevBlock() != -1 && block.getNextBlock() == -1)
            {
                Block prev = readBlock(block.getPrevBlock());
                prev.setNextBlock(-1);
                writeBlock(prev);
                block.setPrevBlock(-1);
            }
            // in between
            else if(block.getPrevBlock() != -1 && block.getNextBlock() != -1)
            {
                Block prev = readBlock(block.getPrevBlock());
                Block next = readBlock(block.getNextBlock());
                prev.setNextBlock(next.getId().getBlockId());
                writeBlock(prev);
                next.setPrevBlock(prev.getId().getBlockId());
                writeBlock(next);
                block.setNextBlock(-1);
                block.setPrevBlock(-1);
            }   
        }
        
        ByteBuffer buffer = ByteBuffer.wrap(block.toByteArray());
        fileChannel.write(buffer, offset);
        block.setDirty(false);
    }

    private BlockId allocateBlock() throws IOException {
        int blockId = ++numberOfBlocks;
        Block block = new Block(new BlockId(schema.getId(), blockId), schema);
        writeBlock(block);
        return block.getId();
    }

    private BlockId getNextFreeBlock(BufferManager bufferManager) throws IOException {
        return getNextFreeBlock(bufferManager, 1);
    }

    private BlockId getNextFreeBlock(BufferManager bufferManager, final int numberOfRecords) throws IOException {
        int nextBlockId = firstFreeBlock;
        while(nextBlockId != -1)
        {
            Block block = bufferManager.pinBlock(new BlockId(schema.getId(), nextBlockId));// readBlock(nextBlockId);
            bufferManager.unpinBlock(block);
            if(block.getFreeSpace() >= numberOfRecords)
            {
                return block.getId();
            }
            nextBlockId = block.getNextBlock();
        }
        return allocateBlock();
    }

    public void insertRecord(BufferManager bufferManager, Record record) throws IOException {
        BlockId blockId = getNextFreeBlock(bufferManager);
        Block block = bufferManager.pinBlock(blockId);
        block.insert(block.getNextFreeSlot(), record);
    }

    public void deleteRecord(Record record) throws IOException {
        RecordId recId = record.getRecordId();
        if(recId.getBlockId().getSchemaId() != schema.getId())
            throw new DbException("DbFile.deleteRecord: wrong schema");
        Block block = readBlock(recId.getBlockId());
        block.delete(recId.getSlot());
    }

    public File getFile() {
        return file;
    }

    public Schema getSchema() {
        return schema;
    }

    public int hashCode() {
        return file.getAbsoluteFile().hashCode();
    }

    public DbFileIterator iterator(BufferManager bufferManager) {
        return new DbFileIterator(this, bufferManager);
    }

    private void saveHeader() throws IOException {
        byte[] header = new byte[Block.BLOCK_SIZE];
        ByteBuffer buffer = ByteBuffer.allocate(Block.BLOCK_SIZE);
        buffer.putInt(0, numberOfBlocks);
        buffer.putInt(4, firstFreeBlock);
        buffer.putInt(8, schema.getId());
        fileChannel.write(buffer, 0);
    }

    private void readHeader() throws IOException {
        ByteBuffer buffer = ByteBuffer.allocate(Block.BLOCK_SIZE);
        fileChannel.read(buffer, 0);
        numberOfBlocks = buffer.getInt(0);
        firstFreeBlock = buffer.getInt(4);
        int schemaId = buffer.getInt(8);
        //schema = Database.getSchema(schemaId);
    }
    
    public void flush() throws IOException
    {
        saveHeader();
    }

    public void close() throws IOException {
        saveHeader();
        fileChannel.close();
    }
}
