package storage;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 *
 * @author bulskov
 */
public class BlockId {
    public static final int SIZE = (Integer.SIZE * 2)/Byte.SIZE;
    private final int blockId;
    private final int schemaId;
    
    public BlockId(int schemaId, int blockId)
    {
        this.schemaId = schemaId;
        this.blockId = blockId;
    }
    
    public BlockId(DataInputStream dis) throws IOException
    {
        blockId = dis.readInt();
        schemaId = dis.readInt();
    }

    public int getBlockId() {
        return blockId;
    }

    public int getSchemaId() {
        return schemaId;
    }

    @Override
    public int hashCode() {
       int mask = 0x0000FFFF;
        int hash = (schemaId & mask) << 16;
        hash = hash | (blockId & mask);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BlockId other = (BlockId) obj;
        if (this.blockId != other.blockId) {
            return false;
        }
        if (this.schemaId != other.schemaId) {
            return false;
        }
        return true;
    }
    
    public void write(DataOutputStream dos) throws IOException
    {
        dos.writeInt(blockId);
        dos.writeInt(schemaId);
    }
}
