package storage;

import catalog.DataType;
import database.DbException;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 *
 * @author bulskov
 */
public class IntegerField implements Field
{

    private int value;
    private DataType type;

    public IntegerField(final int value)
    {
        this.value = value;
        type = DataType.INTEGER;
    }

    public IntegerField(DataInputStream dis) throws IOException
    {
        this(dis.readInt());
    }

    @Override
    public void write(DataOutputStream dos) throws IOException
    {
        dos.writeInt(value);
    }

    @Override
    public DataType getType()
    {
        return type;
    }

    @Override
    public int hashCode()
    {
        return value;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final IntegerField other = (IntegerField) obj;
        if (this.value != other.value)
        {
            return false;
        }
        return true;
    }

    @Override
    public Object getValue()
    {
        return value;
    }
    
    @Override
    public int compareTo(Field field)
    {
        switch(field.getType())
        {
            case INTEGER:
                return ((Integer)value).compareTo((Integer)field.getValue());
            case FLOAT:
                return  ((Float)field.getValue()).compareTo(new Float((Integer)value));
            default:
                throw new DbException("IntegerField.compareTo: not comparable types " +
                        type.name() + "compareTo(" + field.getType().name() + ")");
        }
    }
}
