package storage;

import database.Schema;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.BitSet;

/**
 *
 * @author bulskov
 */
public class Block
{

    public static final int BLOCK_SIZE = 1024;
    private final BlockId blockId;
    private final Record[] records;
    private final BitSet freeMap;
    private int nextBlock = -1;
    private int prevBlock = -1;
    private boolean dirty;
    private int pinCount = 0;
    //private int recordSizeInBits = 0;

    public Block(final BlockId blockId, Schema schema)
    {
        this.blockId = blockId;
        int recIdSize = RecordId.SIZE;
        int schemaSize = schema.size();
        int recordSizeInBits = (schemaSize + recIdSize) * Byte.SIZE;
        int numberOfRecords = computeNumberOfRecords(recordSizeInBits);
        records = new Record[numberOfRecords];
        freeMap = new BitSet(numberOfRecords);
        freeMap.set(0, numberOfRecords);
    }

    public Block(final byte[] data, final Schema schema) throws IOException
    {
        DataInputStream dis = new DataInputStream(new ByteArrayInputStream(data));
        blockId = new BlockId(dis);
        int numberOfRecords = dis.readShort();
        records = new Record[numberOfRecords];
        int freeMapSize = dis.readShort();
        byte[] freeMapData = new byte[freeMapSize];
        dis.read(freeMapData);
        freeMap = BitSet.valueOf(freeMapData);
        nextBlock = dis.readInt();
        prevBlock = dis.readInt();

        for (int i = 0; i < numberOfRecords; i++)
        {
            if (!freeMap.get(i))
            {
                records[i] = new Record(schema, dis);
            }
        }
    }

    public byte[] toByteArray() throws IOException
    {
        ByteArrayOutputStream baos = new ByteArrayOutputStream(BLOCK_SIZE);
        DataOutputStream dos = new DataOutputStream(baos);
        blockId.write(dos);
        dos.writeShort(records.length);
        byte[] freeMapByteArray = freeMap.toByteArray();
        dos.writeShort(freeMapByteArray.length);
        dos.write(freeMapByteArray);
        dos.writeInt(nextBlock);
        dos.writeInt(prevBlock);

        for (int i = 0; i < records.length; i++)
        {
            if (!freeMap.get(i))
            {
                records[i].write(dos);
            }
        }
        dos.flush();

        return baos.toByteArray();
    }

    public BlockId getId()
    {
        return blockId;
    }

    public Record delete(final int slot)
    {
        checkSlot(slot);
        if (freeMap.get(slot))
        {
            throw new IllegalArgumentException("Block.delete: slot " + slot + " is already empty");
        }
        Record deletedRecord = records[slot];
        freeMap.set(slot);
        dirty = true;
        return deletedRecord;
    }

    public Record insert(final int slot, final Record record)
    {
        checkSlot(slot);
        if (!freeMap.get(slot))
        {
            throw new IllegalArgumentException("Block.insert: slot " + slot + " is in use");
        }
        records[slot] = record;
        record.setRecordId(new RecordId(blockId, slot));
        freeMap.clear(slot);
        dirty = true;
        return record;
    }

    public Record update(final int slot, final Record record)
    {
        checkSlot(slot);
        Record updatedRecord = records[slot];
        records[slot] = record;
        dirty = true;
        return updatedRecord;
    }

    public Record getRecord(final int slot)
    {
        checkSlot(slot);
        if (freeMap.get(slot))
        {
            throw new IllegalArgumentException("Block.getRecord: slot " + slot + " is empty");
        }
        return records[slot];
    }

    public boolean hasFreeSpace()
    {
        return freeMap.cardinality() > 0;
    }

    public int getFreeSpace()
    {
        return freeMap.cardinality();
    }

    public int getNextFreeSlot()
    {
        if (!hasFreeSpace())
        {
            throw new IllegalArgumentException("Block.getNextFreeSlot: no free slots available");
        }
        return freeMap.nextSetBit(0);
    }

    private void checkSlot(final int slot) throws IllegalArgumentException
    {
        if ((short) slot != slot)
        {
            throw new IllegalArgumentException("Block: slot " + slot + " is not valid");
        }
    }

    private int computeNumberOfRecords(final int recordSize)
    {
        // recordSize = number of bits needed for a record
        int recSpace = recordSize + 1; // need 1 for the freelist
        int sizeOfHeaderInfo = 8 + 2 + 2 + 4 + 4; // blockId, numberOfRecords, size of free map, next, prev
        int size = (BLOCK_SIZE - sizeOfHeaderInfo) * Byte.SIZE;
        return (size / recSpace);
    }

    public BlockIterator iterator()
    {
        return new BlockIterator(this);
    }

    public int getNumberOfRecords()
    {
        return records.length;
    }

    public boolean isValid(int blockId)
    {
        return !freeMap.get(blockId);
    }

    public void setDirty(boolean dirty)
    {
        this.dirty = dirty;
    }

    public boolean isDirty()
    {
        return dirty;
    }

    public int getNextBlock()
    {
        return nextBlock;
    }

    public void setNextBlock(int nextBlock)
    {
        this.nextBlock = nextBlock;
        dirty = true;
    }

    public int getPrevBlock()
    {
        return prevBlock;
    }

    public void setPrevBlock(int prevBlock)
    {
        this.prevBlock = prevBlock;
        dirty = true;
    }

    public int getPinCount()
    {
        return pinCount;
    }

    public void incrementPinCount()
    {
        this.pinCount++;
    }

    public void decrementPinCount()
    {
        this.pinCount--;
    }
}
