package net.palmund.query;

import database.Database;
import org.junit.Assert;
import org.junit.Test;
import query.FromEnumerator;
import storage.Record;

import java.util.List;
import java.util.Map;

/**
 * Created by palmund on 21/03/14.
 */
public class HashAlgorithmTest {
    private final String TEST_TABLE_1 = "hashjoin_test1";
    private final String TEST_TABLE_2 = "hashjoin_test2";
    private final Database database = new Database();

    @Test
    public void partitionRecords_SingleColumn() throws Exception {
        final FromEnumerator query = database.from(TEST_TABLE_1);
        final HashAlgorithm algorithm = new HashAlgorithm();

        final Map<Integer, List<Record>> partitionedRecords = algorithm.partitionRecords(query, query.getSchema(), "dept_id");

        Assert.assertEquals("More keys than was is supposed to", 4, partitionedRecords.keySet().size());
        Assert.assertEquals("Should be: Jones, 33, Engineering -- Heinsenberg, 33, Engineering", 2, partitionedRecords.get(33).size());
    }
}
