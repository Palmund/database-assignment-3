package net.palmund.query;

import database.Database;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import query.Enumerator;
import storage.Record;

import static evaluation.Operations.column;

/**
 * Created by palmund on 21/03/14.
 */
public class HashJoinEnumeratorTest {
    private final String TEST_TABLE_1 = "hashjoin_test1";
    private final String TEST_TABLE_2 = "hashjoin_test2";
    private final Database database = new Database();

    @Before
    public void setUp() throws Exception {
//        database.createTable(TEST_TABLE_1, Attribute.createString("last_name", 30), Attribute.createInteger("dept_id"));
//        database.createTable(TEST_TABLE_2, Attribute.createString("dept_name", 30), Attribute.createInteger("dept_id"));
//        database.commit();

//        database.insertInto(TEST_TABLE_1, "Jones", 33);
//        database.insertInto(TEST_TABLE_1, "Rafferty", 31);
//        database.insertInto(TEST_TABLE_1, "Robinson", 34);
//        database.insertInto(TEST_TABLE_1, "Smith", 34);
//        database.insertInto(TEST_TABLE_1, "John", 0);
//        database.insertInto(TEST_TABLE_1, "Heisenberg", 33);
//        database.commit();

//        database.insertInto(TEST_TABLE_2, "Engineering", 33);
//        database.insertInto(TEST_TABLE_2, "Sales", 31);
//        database.insertInto(TEST_TABLE_2, "Clerical", 34);
//        database.commit();
    }

    @After
    public void tearDown() throws Exception {
        //        database.dropTable(TEST_TABLE_1);
        //
        //        database.dropTable(TEST_TABLE_2);
        //
        //        database.close();
        //
        //        database.from(TEST_TABLE_1);
    }

    @Test
    public void name() throws Exception {
        final Enumerator enumerator = database
                .from(TEST_TABLE_1)
                .hashJoin(database.from(TEST_TABLE_2), column(TEST_TABLE_1 + ".dept_id").isEqualTo(column(TEST_TABLE_2 + ".dept_id")));

        enumerator.open();
        int i = 0;
        while (enumerator.hasNext()) {
            final Record record = enumerator.next();
            i += 1;
        }
        Assert.assertEquals(5, i);
    }
}