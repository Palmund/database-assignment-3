package net.palmund.query;

import database.Schema;
import query.Enumerator;
import storage.Record;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by palmund on 21/03/14.
 */
public class HashAlgorithm {
    public Map<Integer, List<Record>> partitionRecords(Enumerator source, Schema schema, String column) {
        final HashMap<Integer, List<Record>> map = new HashMap<>();
        column = column.replaceFirst(schema.getName()+".", "");

        source.open();
        source.rewind();

        while (source.hasNext()) {
            final Record next = source.next();
            final Object data = next.getData(schema.indexOf(column));
            final int code = data.hashCode();
            if (!map.containsKey(code)) {
                map.put(code, new ArrayList<Record>());
            }
            final List<Record> records = map.get(code);
            records.add(next);
        }

        return map;
    }
}