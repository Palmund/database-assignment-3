package net.palmund.query;

import evaluation.HashJoinPredicate;
import evaluation.Predicate;
import query.Enumerator;
import storage.Record;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by palmund on 21/03/14.
 */
public class HashJoinEnumerator extends AbstractJoinEnumerator {

    private final Map<Integer, List<Record>> hashedRecords;
    private int index = 0;
    private List<Record> records;

    public HashJoinEnumerator(Enumerator source, Enumerator joiningSource, Predicate operation) {
        super(source, joiningSource, new HashJoinPredicate(operation));
        hashedRecords = new HashMap<>();
    }

    @Override
    protected Record fetchNext() {
        if (!validMergedSchema) {
            return null;
        }
        if (records == null) {
            hashRecords();
        }
        if (index < records.size()) {
            return records.get(index++);
        }
        return null;
    }

    private void hashRecords() {
        records = new ArrayList<>();
        final Object leftColumn = operation.getLeft();
        final Map<Integer, List<Record>> hashedLeft = hashSource(source, (String) leftColumn);
        final Object rightColumn = operation.getRight();
        final Map<Integer, List<Record>> hashedRight = hashSource(joiningSource, (String) rightColumn);
        for (Integer key : hashedLeft.keySet()) {
            final List<Record> recordList = hashedRight.get(key);
            if (recordList != null) {
                // Record in left
                for (Record leftRecord : hashedLeft.get(key)) {
                    for (Record rightRecord : recordList) {
                        final Record joinedRecord = leftRecord.join(rightRecord, getSchema());
                        records.add(joinedRecord);
                    }
                }
            }
        }
    }

    private Map<Integer, List<Record>> hashSource(Enumerator source, String column) {
        final HashAlgorithm algorithm = new HashAlgorithm();
        return algorithm.partitionRecords(source, source.getSchema(), column);
    }
}