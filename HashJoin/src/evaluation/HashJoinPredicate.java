package evaluation;

import catalog.DataType;
import storage.Record;

/**
 * Created by palmund on 21/03/14.
 */
public class HashJoinPredicate extends Predicate {
    public HashJoinPredicate(Operator operator, DataType leftType, Object left, DataType rightType, Object right) {
        super(operator, leftType, left, rightType, right);
    }

    public HashJoinPredicate(Predicate operation) {
        super(operation.operator, operation.leftType, operation.left, operation.rightType, operation.right);
    }

    public Object getLeftValue(Record record) {
        return leftType == DataType.FIELDNO ?
                record.getData((Integer) left) :
                left;
    }

    public Object getRightValue(Record record) {
        return rightType == DataType.FIELDNO ?
                record.getData((Integer) right) :
                right;
    }
}