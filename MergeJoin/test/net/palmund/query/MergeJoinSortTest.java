package net.palmund.query;

import database.Database;
import org.junit.Assert;
import org.junit.Test;
import query.FromEnumerator;
import storage.Record;

import java.util.ArrayList;

/**
 * Created by palmund on 24/03/14.
 */
public class MergeJoinSortTest {
    private final Database database = new Database();

    private final String TEST_TABLE_1 = "mergejoin_test1";
    private final String TEST_TABLE_2 = "mergejoin_test2";

    @Test
    public void sort_verifyCount() throws Exception {
        final FromEnumerator table = database.from(TEST_TABLE_1);
        final ArrayList<Record> sortedList = new MergeJoinSort().sort(table, 0);

        Assert.assertEquals(7, sortedList.size());
    }

    @Test
    public void sort_verifyOrder() throws Exception {
        final FromEnumerator table = database.from(TEST_TABLE_1);
        final ArrayList<Record> sortedList = new MergeJoinSort().sort(table, 0);

        Assert.assertEquals("a", sortedList.get(0).getData(0));
        Assert.assertEquals("b", sortedList.get(1).getData(0));
        Assert.assertEquals("d", sortedList.get(2).getData(0));
        Assert.assertEquals("d", sortedList.get(3).getData(0));
        Assert.assertEquals("f", sortedList.get(4).getData(0));
        Assert.assertEquals("m", sortedList.get(5).getData(0));
        Assert.assertEquals("q", sortedList.get(6).getData(0));
    }
}