package net.palmund.query;

import database.Database;
import evaluation.Operator;
import evaluation.Predicate;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by palmund on 24/03/14.
 */
public class MergeJoinEnumeratorTest {
    private final String TEST_TABLE_1 = "mergejoin_test1";
    private final String TEST_TABLE_2 = "mergejoin_test2";
    private final Database database = new Database();

    @Before
    public void setUp() throws Exception {
//          database.createTable(TEST_TABLE_1, Attribute.createString("char", 2), Attribute.createInteger("number"));
//          database.createTable(TEST_TABLE_2, Attribute.createString("char", 2), Attribute.createString("char2", 2));
//          database.commit();

//          database.insertInto(TEST_TABLE_1, "a", 3);
//          database.insertInto(TEST_TABLE_1, "f", 7);
//          database.insertInto(TEST_TABLE_1, "q", 6);
//          database.insertInto(TEST_TABLE_1, "d", 13);
//          database.insertInto(TEST_TABLE_1, "m", 5);
//          database.insertInto(TEST_TABLE_1, "d", 8);
//          database.insertInto(TEST_TABLE_1, "b", 1);
//          database.commit();

//          database.insertInto(TEST_TABLE_2, "a", "A");
//          database.insertInto(TEST_TABLE_2, "b", "G");
//          database.insertInto(TEST_TABLE_2, "c", "L");
//          database.insertInto(TEST_TABLE_2, "d", "N");
//          database.insertInto(TEST_TABLE_2, "m", "B");
//          database.commit();
    }

    @After
    public void tearDown() throws Exception {
        //        database.dropTable(TEST_TABLE_1);
        //
        //        database.dropTable(TEST_TABLE_2);
        //
        //        database.close();
        //
        //        database.from(TEST_TABLE_1);
    }

    @Test
    public void join() throws Exception {
        final MergeJoinEnumerator query = database
                .from(TEST_TABLE_1)
                .mergeJoin(database.from(TEST_TABLE_2), (Predicate) Predicate.compareColumnColumn(Operator.EQ, TEST_TABLE_1 + ".char", TEST_TABLE_2 + ".char"));

        query.open();
        int i = 0;
        while (query.hasNext()) {
            query.next();
        }
        Assert.assertEquals(2, i);
    }
}