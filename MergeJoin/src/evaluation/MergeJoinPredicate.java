package evaluation;

import catalog.DataType;
import storage.Record;

/**
 * Created by palmund on 24/03/14.
 */
public class MergeJoinPredicate extends Predicate {
    public MergeJoinPredicate(Operator operator, DataType leftType, Object left, DataType rightType, Object right) {
        super(operator, leftType, left, rightType, right);
    }

    public MergeJoinPredicate(Predicate operation) {
        super(operation.operator, operation.leftType, operation.left, operation.rightType, operation.right);
    }

    public Object getLeftValue(Record record) {
        if (leftType == DataType.COLNAME) {
            final String sleft = (String) left;
            final String columnName = sleft.substring(sleft.indexOf(".") + 1);
            return record.getSchema().indexOf(columnName);
        } else {
            return leftType == DataType.FIELDNO ?
                    record.getData((Integer) left) :
                    left;
        }
    }

    public Object getRightValue(Record record) {
        return rightType == DataType.FIELDNO ?
                record.getData((Integer) right) :
                right;
    }
}
