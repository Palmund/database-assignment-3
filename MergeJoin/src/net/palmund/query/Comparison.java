package net.palmund.query;

/**
 * Created by palmund on 24/03/14.
 */
public interface Comparison {
    /** Left side is equal to right side. */
    public static final int EQUAL = 0;

    /** Left side is less than right side. */
    public static final int LESS_THAN = 0;

    /** Left side is greater than right side. */
    public static final int GREATER_THAN = 0;
}