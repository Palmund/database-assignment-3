package net.palmund.query;

import evaluation.MergeJoinPredicate;
import evaluation.Predicate;
import query.Enumerator;
import storage.Record;

import java.util.ArrayList;

/**
 * Created by palmund on 24/03/14.
 */
public class MergeJoinEnumerator extends AbstractJoinEnumerator {
    private boolean firstRun = true;

    public MergeJoinEnumerator(Enumerator source, Enumerator joiningSource, Predicate operation) {
        super(source, joiningSource, new MergeJoinPredicate(operation));
    }

    @Override
    protected Record fetchNext() {
        if (firstRun) {
            final ArrayList<Record> sortedSource = sortSourceRelation();
            final ArrayList<Record> sortedJoiningSource = sortJoiningSourceRelation();
        }
        return super.fetchNext();
    }

    private ArrayList<Record> sortJoiningSourceRelation() {
        joiningSource.open();
        joiningSource.rewind();
        int left = 0;
        if (joiningSource.hasNext()) {
            final Record next = joiningSource.next();
            left = ((Integer) ((MergeJoinPredicate) operation).getLeftValue(next));
        }
        return new MergeJoinSort().sort(joiningSource, left);
    }

    private ArrayList<Record> sortSourceRelation() {
        source.open();
        source.rewind();
        int left = 0;
        if (source.hasNext()) {
            final Record next = source.next();
            left = ((Integer) ((MergeJoinPredicate) operation).getLeftValue(next));
        }
        return new MergeJoinSort().sort(source, left);
    }
}