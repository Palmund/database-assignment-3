package net.palmund.query;

import query.Enumerator;
import storage.Record;

import java.util.ArrayList;

/**
 * Created by palmund on 24/03/14.
 */
public class MergeJoinSort {

    public ArrayList<Record> sort(Enumerator enumerator, int on) {
        ArrayList<Record> list = new ArrayList<>();

        enumerator.open();
        enumerator.rewind();

        while (enumerator.hasNext()) {
            final Record next = enumerator.next();
            final Object nextData = next.getData(on);

            int insertIndex = -1;
            for (int i = 0; i < list.size(); i++) {
                final Record record = list.get(i);
                final Object recordData = record.getData(on);
                if (nextData instanceof Comparable<?>) {
                    Comparable nextDataComparable = (Comparable) nextData;
                    final int compareTo = nextDataComparable.compareTo(recordData);
                    if (compareTo < Comparison.LESS_THAN) {
                        insertIndex = i;
                        break;
                    }
                }
            }
            if (insertIndex == -1) {
                list.add(next);
            } else {
                list.add(insertIndex, next);
            }
        }

        return list;
    }
}